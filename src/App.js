import './App.css';
import Header from './components/Header';
import SignupForm from './components/SignupForm';

function App() {
  return (
    <div className="App">
      <Header />

      <SignupForm />

    </div>
  );
}

export default App;
