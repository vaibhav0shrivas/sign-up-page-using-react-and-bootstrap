import React from "react";
import validator from 'validator';

class SignupForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            inputFirstName: null,
            inputLastName: null,
            inputAge: null,
            inputGender: null,
            inputRole: null,
            inputEmail: null,
            inputPassword: null,
            inputRepeatPassword: null,
            tosCheckbox: null,
            isFormValid: null,
            inputFirstNameIsValid: null,
            inputLastNameIsValid: null,
            inputAgeIsValid: null,
            inputGenderIsValid: null,
            inputRoleIsValid: null,
            inputEmailIsValid: null,
            inputPasswordIsValid: null,
            inputRepeatPasswordIsValid: null,
            tosCheckboxIsValid: null,
            onSubmitMessage: "Please enter following details-",
            invalidInputMessage: {
                forInputPassword: null,
            }
        };
    };


    validateFormInput = (inputElementId) => {

        switch (inputElementId) {

            case "inputFirstName":
                if (validator.isEmpty(this.state.inputFirstName) === true ||
                    validator.isAlpha(this.state.inputFirstName) === false) {
                    this.setState({
                        isFormValid: false,
                        inputFirstNameIsValid: false,

                    });
                } else {
                    this.setState({
                        inputFirstNameIsValid: true,
                    });
                }

                break;
            case "inputLastName":
                if (validator.isEmpty(this.state.inputLastName) === true ||
                    validator.isAlpha(this.state.inputLastName) === false) {
                    this.setState({
                        isFormValid: false,
                        inputLastNameIsValid: false,
                    });
                } else {
                    this.setState({
                        inputLastNameIsValid: true,
                    });
                }
                break;
            case "inputAge":
                if (validator.isEmpty(this.state.inputAge) === true ||
                    validator.isInt(this.state.inputAge,{ min: 14, max: 75 }) === false ) {
                    this.setState({
                        isFormValid: false,
                        inputAgeIsValid: false,
                    });
                } else {
                    this.setState({
                        inputAgeIsValid: true,
                    });
                }
                break;
            case "inputGender":
                if (this.state.inputGender === "Choose...") {
                    this.setState({
                        isFormValid: false,
                        inputGenderIsValid: false,
                    });
                } else {
                    this.setState({
                        inputGenderIsValid: true,
                    });
                }
                break;
            case "inputRole":
                if (this.state.inputRole === "Choose...") {
                    this.setState({
                        isFormValid: false,
                        inputRoleIsValid: false,
                    });
                } else {
                    this.setState({
                        inputRoleIsValid: true,
                    });
                }
                break;
            case "inputEmail":
                if (validator.isEmail(this.state.inputEmail) === false ||
                    validator.isEmpty(this.state.inputEmail) === true) {
                    this.setState({
                        isFormValid: false,
                        inputEmailIsValid: false,
                    });
                } else {
                    this.setState({
                        inputEmailIsValid: true,
                    });
                }
                break;
            case "inputPassword":

                if (validator.isStrongPassword(this.state.inputPassword) === false ||
                    validator.isEmpty(this.state.inputPassword) === true) {

                    let passwordErrorString = "";

                    if (validator.isEmpty(this.state.inputPassword) === true) {
                        passwordErrorString += "Enter a password, Use at least 8 characters. Use a mix of letters (uppercase and lowercase), numbers, and symbols!";

                    } else {
                        passwordErrorString = "Password must have";
                        if (this.state.inputPassword.length < 8) {
                            passwordErrorString += " at least 8 characters,";
                        }

                        if (/[0-9]/.test(this.state.inputPassword) === false) {
                            passwordErrorString += " at least 1 number,";
                        }

                        if (/[a-z]/.test(this.state.inputPassword) === false) {
                            passwordErrorString += " at least 1 lowercase character,";
                        }


                        if (/[A-Z]/.test(this.state.inputPassword) === false) {
                            passwordErrorString += " at least 1 uppercase character,";
                        }

                        if (/[-’/`~!#*$@_%+=.,^&(){}[\]|;:”<>?\\]/.test(this.state.inputPassword) === false) {
                            passwordErrorString += " at least 1 special character,";
                        }


                    }

                    this.setState((prevState) => {
                        return {
                            isFormValid: false,
                            inputPasswordIsValid: false,
                            invalidInputMessage: {
                                ...prevState.invalidInputMessage,
                                forInputPassword: passwordErrorString,
                            }
                        }
                    });
                } else {
                    this.setState({
                        inputPasswordIsValid: true,
                    });
                }

                break;
            case "inputRepeatPassword":
                if (this.state.inputPasswordIsValid === true && this.state.inputPassword === this.state.inputRepeatPassword) {
                    this.setState({
                        inputRepeatPasswordIsValid: true,
                    });
                } else {
                    this.setState({
                        isFormValid: false,
                        inputRepeatPasswordIsValid: false,
                    });
                }
                break;
            case "tosCheckbox":
                if (this.state.tosCheckbox === true) {
                    this.setState({
                        tosCheckboxIsValid: true,
                    });
                } else {
                    this.setState({
                        tosCheckboxIsValid: false,
                        isFormValid: false,
                    });
                }
                break;
            default:
                console.error(`${inputElementId}ID does not match state variables!`);
        };


    };

    handleUserInput = (event) => {

        if (event.target.id === "tosCheckbox") {

            this.setState({
                [event.target.id]: event.target.checked,

            },
                () => {
                    this.validateFormInput(event.target.id);

                }
            );

        } else {

            this.setState({
                [event.target.id]: event.target.value,

            },
                () => {
                    this.validateFormInput(event.target.id);

                }
            );
        }

    };

    displayErrors = () => {
        this.setState((prevState) => {

            return {
                inputFirstNameIsValid: (prevState.inputFirstNameIsValid === true),
                inputLastNameIsValid: (prevState.inputLastNameIsValid === true),
                inputAgeIsValid: (prevState.inputAgeIsValid === true),
                inputGenderIsValid: (prevState.inputGenderIsValid === true),
                inputRoleIsValid: (prevState.inputRoleIsValid === true),
                inputEmailIsValid: (prevState.inputEmailIsValid === true),
                inputPasswordIsValid: (prevState.inputPasswordIsValid === true),
                inputRepeatPasswordIsValid: (prevState.inputRepeatPasswordIsValid === true),
                tosCheckboxIsValid: (prevState.tosCheckboxIsValid === true),
            };

        });
    }

    sendSignupForm = (event) => {
        event.preventDefault();
        this.displayErrors();

        let isFormValid = false
        if (this.state.inputFirstNameIsValid &&
            this.state.inputLastNameIsValid &&
            this.state.inputAgeIsValid &&
            this.state.inputGenderIsValid &&
            this.state.inputRoleIsValid &&
            this.state.inputEmailIsValid &&
            this.state.inputPasswordIsValid &&
            this.state.inputRepeatPasswordIsValid &&
            this.state.tosCheckboxIsValid
        ) {
            isFormValid = true;

        }

        if (isFormValid === true) {
            this.setState({
                isFormValid: true,
                onSubmitMessage: "Sign up done. Please check your email!"
            },
                () => {
                    console.log("Sending Form");
                    //api call
                }
            );

        } else {
            this.setState({
                onSubmitMessage: "Invalid form inputs, please fill every detail properly!",
            },
                () => {
                    console.log("Invalid Form");
                }
            );
        }


    };

    render() {
        return (
            <section className="col-md-6 col-10 mx-auto my-2 bg-dark text-light p-2 rounded">
                <form className="row g-3 p-3">
                    <div className="col-md-12">
                        <h3>{this.state.onSubmitMessage}</h3>
                    </div>
                    <div className="col-md-6">
                        <label htmlFor="inputFirstName"
                            className="form-label"><i className="fa-solid fa-monument"></i> First name</label>
                        <input type="text"
                            className={
                                this.state.inputFirstNameIsValid === false ?
                                    "form-control is-invalid" :
                                    this.state.inputFirstNameIsValid === true ?
                                        "form-control is-valid" : "form-control"
                            }
                            id="inputFirstName"
                            onChange={(event) => { this.handleUserInput(event) }}
                        />
                        <div className="invalid-feedback">
                            Please provide only english alphabets without space for First Name!
                        </div>
                        <div className="valid-feedback">
                            Looks good!
                        </div>
                    </div>
                    <div className="col-md-6">
                        <label htmlFor="inputLastName"
                            className="form-label"><i className="fa-solid fa-monument"></i> Last name</label>
                        <input type="text"
                            className={
                                this.state.inputLastNameIsValid === false ?
                                    "form-control is-invalid" :
                                    this.state.inputLastNameIsValid === true ?
                                        "form-control is-valid" : "form-control"
                            }
                            id="inputLastName"
                            onChange={(event) => { this.handleUserInput(event) }}
                        />
                        <div className="invalid-feedback">
                            Please provide only english alphabets without space for Last Name!
                        </div>
                        <div className="valid-feedback">
                            Looks good!
                        </div>
                    </div>
                    <div className="col-md-6">
                        <label htmlFor="inputAge"
                            className="form-label"><i className="fa-solid fa-arrow-up-9-1"></i> Age</label>
                        <input type="number"
                            className={
                                this.state.inputAgeIsValid === false ?
                                    "form-control is-invalid" :
                                    this.state.inputAgeIsValid === true ?
                                        "form-control is-valid" : "form-control"
                            }
                            id="inputAge"
                            onChange={(event) => { this.handleUserInput(event) }}
                        />
                        <div className="invalid-feedback">
                            Your age should be a non-decimal number between 14 and 75 to work legally!
                        </div>
                        <div className="valid-feedback">
                            Looks good!
                        </div>
                    </div>
                    <div className="col-md-6">
                        <label htmlFor="inputGender"
                            className="form-label"><i className="fa-solid fa-venus-mars"></i> Gender</label>
                        <select id="inputGender"
                            className={
                                this.state.inputGenderIsValid === false ?
                                    "form-control is-invalid" :
                                    this.state.inputGenderIsValid === true ?
                                        "form-control is-valid" : "form-control"
                            }
                            onChange={(event) => { this.handleUserInput(event) }}
                        >
                            <option defaultValue>Choose...</option>
                            <option>Male</option>
                            <option>Female</option>
                            <option>Non Binary</option>
                            <option>Prefer not to say</option>
                        </select>
                        <div className="invalid-feedback">
                            Please choose one of the valid options, apart from Choose...!
                        </div>
                        <div className="valid-feedback">
                            Looks good!
                        </div>
                    </div>
                    <div className="col-md-6">
                        <label htmlFor="inputRole"
                            className="form-label"><i className="fa-solid fa-briefcase"></i> Role</label>
                        <select id="inputRole"
                            className={
                                this.state.inputRoleIsValid === false ?
                                    "form-control is-invalid" :
                                    this.state.inputRoleIsValid === true ?
                                        "form-control is-valid" : "form-control"
                            }
                            onChange={(event) => { this.handleUserInput(event) }}
                        >
                            <option defaultValue>Choose...</option>
                            <option>Developer</option>
                            <option>Senior Developer</option>
                            <option>Lead Engineer</option>
                            <option>CTO</option>
                        </select>
                        <div className="invalid-feedback">
                            Please choose one of the valid options, apart from Choose...!
                        </div>
                        <div className="valid-feedback">
                            Looks good!
                        </div>
                    </div>

                    <div className="col-md-6">
                        <label htmlFor="inputEmail"
                            className="form-label"><i className="fa-solid fa-envelope"></i> Email</label>
                        <input type="email"
                            className={
                                this.state.inputEmailIsValid === false ?
                                    "form-control is-invalid" :
                                    this.state.inputEmailIsValid === true ?
                                        "form-control is-valid" : "form-control"
                            }
                            id="inputEmail"
                            onChange={(event) => { this.handleUserInput(event) }}
                        />
                        <div className="invalid-feedback">
                            Please enter valid email like username@domianname!
                        </div>
                        <div className="valid-feedback">
                            Looks good!
                        </div>
                    </div>
                    <div className="col-md-6">
                        <label htmlFor="inputPassword"
                            className="form-label"><i className="fa-solid fa-lock"></i> Password</label>
                        <input type="password"
                            className={
                                this.state.inputPasswordIsValid === false ?
                                    "form-control is-invalid" :
                                    this.state.inputPasswordIsValid === true ?
                                        "form-control is-valid" : "form-control"
                            }
                            id="inputPassword"
                            onChange={(event) => { this.handleUserInput(event) }}
                        />
                        <div className="invalid-feedback">
                            {this.state.invalidInputMessage.forInputPassword}
                        </div>
                        <div className="valid-feedback">
                            Looks good!
                        </div>
                    </div>
                    <div className="col-md-6">
                        <label htmlFor="inputRepeatPassword"
                            className="form-label" ><i className="fa-solid fa-lock"></i> Repeat Password</label>
                        <input type="password"
                            className={
                                this.state.inputRepeatPasswordIsValid === false ?
                                    "form-control is-invalid" :
                                    this.state.inputRepeatPasswordIsValid === true ?
                                        "form-control is-valid" : "form-control"
                            }
                            id="inputRepeatPassword"
                            onChange={(event) => { this.handleUserInput(event) }}
                        />
                        <div className="invalid-feedback">
                            Repeated password should be same as earlier password!
                        </div>
                        <div className="valid-feedback">
                            Looks good!
                        </div>
                    </div>
                    <div className="col-md-6 mt-4">
                        <div className="form-check">
                            <input className={
                                this.state.tosCheckboxIsValid === false ?
                                    "form-check-input is-invalid" :
                                    this.state.tosCheckboxIsValid === true ?
                                        "form-check-input is-valid" : "form-check-input"
                            }
                                type="checkbox" id="tosCheckbox"
                                onChange={(event) => { this.handleUserInput(event) }} />
                            <span>I agree to the <a href='#!'>Terms & Conditions</a></span>
                            <div className="invalid-feedback">
                                You must agree to our Terms and Conditions!
                            </div>
                            <div className="valid-feedback">
                                You agreed to our Terms and Conditions!
                            </div>
                        </div>
                    </div>
                    <div className="col-md-6 mt-4">
                        <button type="submit"
                            className="btn btn-primary"
                            onClick={(event) => { this.sendSignupForm(event) }}>Sign Up</button>
                    </div>
                </form>
            </section>
        );

    };

};


export default SignupForm;

