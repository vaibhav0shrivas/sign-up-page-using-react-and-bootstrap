import React from "react";

class Header extends React.Component {

    render() {
        return (
            <div className="container-fluid bg-dark text-light py-3">
                <header className="text-center">
                    <h1 className="display-5">Signup page <i className="fa-solid fa-user-plus"></i></h1>
                </header>
            </div>
        );

    };

};


export default Header;

